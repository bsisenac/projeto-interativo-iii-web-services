package webservice.rc;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

//Lista ligada implementado interface para serialização
public class ListaLigada implements Serializable{
    Historico h = new Historico();
    private No ini;

    public ListaLigada() {
        ini = null;
    }

    public No getInicio(){
        return this.ini;
    }
  
   public void mostrarProduto(){
        No aux = ini;
        while (aux != null) {
            Produto prod = (Produto)aux.dado;
            aux = aux.prox;
        }
    }

    //Método que imprime a Lista ligada prenchida
    public void imprimirLista() {
        No ini = getInicio();
        No aux = ini;
        while (aux != null) {
            Produto p = (Produto) aux.dado;
            System.out.println("Nome: " + p.getNome());
            System.out.println("Descrição: " + p.getDescricao());
            System.out.println("Preço: " + p.getPreco());
            System.out.println("Preço Parcela: 10x " + p.getPrecoParcela());
            aux = aux.prox;
        }
    }

    public void mostrarLista() {
        No aux = ini;
        while (aux != null) {
            System.out.println(aux.dado.getClass());
            aux = aux.prox;
        }
    }

    //Método que insere o objeto no inicio da lista
    public void insereNoInicio(Produto v) {
        No noNovo = new No(v);
        noNovo.prox = ini;
        ini = noNovo;
    }

    //Método que insere o objeto no inicio da lista
    public void insereNoFim(Produto v) {
        No noNovo = new No(v);
        if (ini == null)
            ini = noNovo;
        else {
            No aux = ini;
            while (aux.prox != null)
                aux = aux.prox;
            aux.prox = noNovo;
        }
    }

    //Método para salvar arquivo Histórico
    public void salvarHistorico() {
        try {

            File arquivoCriado = new File("historico.txt");
            if (!arquivoCriado.exists()) {
                arquivoCriado.createNewFile();
            }
            FileWriter escreverArq = new FileWriter(arquivoCriado);
            BufferedWriter carregarConteudo = new BufferedWriter(escreverArq);
            
            System.err.println("Arquivo salvo com exito.\n");
            System.out.println(arquivoCriado.getAbsolutePath());
        }
        catch (IOException ex) {
            Logger.getLogger(ListaLigada.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}