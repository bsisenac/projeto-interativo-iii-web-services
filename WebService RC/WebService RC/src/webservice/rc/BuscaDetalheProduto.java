package webservice.rc;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class BuscaDetalheProduto {
    
    //construtor
    public BuscaDetalheProduto() {
        System.out.println("Buscando Detalhes do Produto...\n");
    }

    //Segunda busca ao clicar no botão 'vizualisar' na tela inicial
    //Via HttpGet
    //Retorna String
    public Produto buscaPagina(String link) throws IOException {
        Produto p;
        
        DefaultHttpClient httpclient = new DefaultHttpClient(); 
        HttpGet httpGet = new HttpGet(link);
        
        try {
            ResponseHandler<String> manipulador = new BasicResponseHandler();
            String resposta = httpclient.execute(httpGet,manipulador);
            int inicio = resposta.indexOf("main-container");

            p = new Produto();

            resposta = resposta.substring(inicio);

            p.setDescricao(this.Descricao(resposta));
            p.setImagem(this.Imagem(resposta)); 
            p.setNome(this.Nome(resposta));
            p.setPreco(this.Preco(resposta));
            p.setPrecoParcela(this.PrecoParcela(resposta));
            p.setNomeImagem( link.substring( p.getImagem().indexOf("br/") + 3, link.indexOf(".html")));
            baixarImagem(p.getImagem(), p.getNomeImagem());

        }
        finally {
            httpGet.releaseConnection();
        }
        return p;
    }

    //Indexa a String do Nome do produto
    private String Nome(String resposta) throws ClientProtocolException,IOException {
        String produto = resposta.substring(resposta.indexOf("<div class=\"product-name\">"), resposta.indexOf("<div class=\"ratings\">"));
        produto = produto.substring(produto.indexOf("<h1>") + "<h1>".length(), produto.indexOf("</h1>"));
        
        return produto;
    }

    //Indexa a String da Descrião do produto
    public String Descricao(String resposta) throws ClientProtocolException,IOException {
        String produto = resposta.substring(resposta.indexOf("<div class=\"product-top\">"), resposta.indexOf("<div class=\"ratings\">"));
        produto = produto.substring(produto.indexOf("<span>") +"<span>".length(), produto.indexOf("</span>"));
        
        produto = produto.replace(",", ",\n");
        produto = produto.replaceAll("\\<.*?\\>", "");
        
        return produto;
    }

    //Indexa a String do Preço do produto
    private String Preco(String resposta) throws ClientProtocolException,IOException {
        String produto = resposta.substring(resposta.indexOf("regular-price"), resposta.indexOf("<!------------------------>"));
        produto = produto.substring(produto.indexOf("R$") + "R$".length() - 2, produto.indexOf("</span>"));

        return produto;
    }

    //Indexa a String do valor da parcela do produto
    private String PrecoParcela(String resposta) throws ClientProtocolException,IOException {
        String produto = resposta.substring(resposta.indexOf("<div class=\"parcelado\">"), resposta.indexOf("<div class=\"featured-promo\">"));
        produto = produto.substring(produto.indexOf("<span class='featured-price'>") + "<span class='featured-price'>".length(), produto.indexOf("<span class=\"sem-juros\">"));
        
        produto = produto.replaceAll("\\<.*?\\>", "");
        
        return produto;
    }
    
    //Indexa a String do link da Imagem do produto
    private String Imagem(String resposta) {
        String produto = resposta.substring(resposta.indexOf("product-img-box"), resposta.indexOf("Duplo clique para aumentar imagem"));
        produto = produto.substring(produto.indexOf("<a href=\"") + "<a href=\"".length() , produto.indexOf(".jpg") +4);
        
        return produto;
    }

    //Método que baixa a Imagem pelo link e grava na pasta 'img' na raiz do projeto
    public void baixarImagem(String urlProduto, String produto) {
        URL url;
        try {
            url = new URL(urlProduto);
            InputStream is = url.openStream();
            OutputStream os = new FileOutputStream("img//" + produto + ".jpg");
            
            byte[] b = new byte[2048];
            int length;
            
            while ((length = is.read(b)) != -1) {
                os.write(b, 0, length);
            }
            
            is.close();
            os.close();
        }
        catch (IOException ex) {
            Logger.getLogger(BuscaDetalheProduto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}