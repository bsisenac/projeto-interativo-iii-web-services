package webservice.rc;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static webservice.rc.Janela.janela;
import static webservice.rc.Janela.painelTopo;
import static webservice.rc.JanelaProduto.*;

public class JanelaPrincipal extends Janela {

    private JMenu menu;
    private JMenuItem menuItem, menuItem2;
    private JMenuBar menuBar;
    public JButton buscar;
    public JButton buttons[];
    public static JTextField texto;
    private ArrayList<Produto> listaProdutos;
    private ArrayList<String> listaImagens;
    private Busca busca;
    
    Historico historico = new Historico();
    ListaLigada lista = new ListaLigada();

    private String links[];
    private String nomes[];

    
    //Método que contrói a Janela Inicial
    public JanelaPrincipal() {
        super("PROJETO INTEGRADOR III | WEB-SERVICE | WWW.WAZ.COM"); 
        this.buscar = new JButton("Buscar");
        buscar.setBackground(new Color(108, 163, 50));
        buscar.setForeground(Color.WHITE);
        texto = new JTextField("Digite o produto que deseja...");
        texto.setPreferredSize(new Dimension(500, 25));
        this.busca = new Busca();
        this.listaProdutos = new ArrayList<Produto>();

        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);

        menuBar = new JMenuBar();
        menu = criarMenu();
        menuBar.add(menu);

        janela.setJMenuBar(menuBar);

        texto.setColumns(30);
        painelTopo.add(texto); 
        painelTopo.add(buscar, BorderLayout.EAST); 

        final String produto[] = new String[45];

        texto.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                texto.setText("");
            }
        });

        texto.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    eventoBotao();
                }
            }
        });

        buscar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                eventoBotao();
            }
        });

        System.out.println("WebService iniciado.");

        JScrollPane scroller = new JScrollPane(painelCentral);
        this.container.add(scroller);

        painelTopo.validate();
        painelCentral.validate();
        janela.setVisible(true);
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buttons = null;
    }

    //Método que monta e exibe os resultados da primeira busca
    public void eventoBotao() {

        if (texto.getText().equals("") || texto.getText().equals("Digite o produto que deseja...")) {
            painelCentral.removeAll();
            painelCentral.repaint();
            painelCentral.revalidate();
            painelCentral.add(new JLabel("Busca Vazia."));
            System.err.println("Busca vazia.");
        } else {
            painelCentral.removeAll();
            painelCentral.repaint();
            painelCentral.revalidate();

            listaProdutos = new ArrayList<Produto>();
            try {
                
                System.out.println("Buscando...\n");
                listaProdutos = busca.BuscaWaz(texto.getText());
                System.out.println("Total de Resultados: " + listaProdutos.size() + "\n");
                
            } catch (IOException ex) {
                Logger.getLogger(JanelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }

            buttons = new JButton[listaProdutos.size()];
            Produto n = new Produto();

            links = new String[listaProdutos.size()];
            nomes = new String[listaProdutos.size()];

            //Repeticao que popula a tabela e os botoes na tela inicial
            for (int i = 0; i < buttons.length; i++) {
                n = (Produto) listaProdutos.get(i);
                buttons[i] = new JButton("VISUALIZAR PRODUTO");
                buttons[i].setPreferredSize(new Dimension(5, 10));
                buttons[i].setBackground(new Color(108, 163, 50));
                buttons[i].setForeground(Color.WHITE);
                String[] colunas = {"Produtos", "Preço"};
                Object[][] dados = {{n.getNome(), n.getPreco()}};
                JTable tabela = new JTable(dados, colunas);

                //tabela.setPreferredSize(new Dimension(5, 15));
                tabela.setEnabled(false);

                painelCentral.add(tabela);
                links[i] = n.getLink();
                nomes[i] = n.getNome();
                painelCentral.add(buttons[i]);
            }
            
            
            for (int i = 0; i < buttons.length; i++) {
                final int num = i;
                
                //Ação para nova busca e abir Janela do Produto ao clicar no botão
                buttons[i].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        try {
                            new JanelaProduto(nomes[num]);
                            

                            //Segunda Busca. Buscando detalhes do produto
                            BuscaDetalheProduto bdp = new BuscaDetalheProduto();

                            Produto pd = bdp.buscaPagina(links[num]);
                            
                            
                            //Ler e salvar Historico utilizando Lista ligada
                            lista = historico.lerArquivo();
                            if( lista== null ) lista = new ListaLigada();
                            lista.insereNoFim(pd);
                            historico.salvarLista(lista);
                            lista.imprimirLista();

                            String descTexto = pd.getDescricao();
                            String nomeTexto = pd.getNome();
                            String precoValor = pd.getPreco();
                            String parcelaValor = pd.getPrecoParcela();

                            String texto = "PRODUTO:\n" + nomeTexto;
                            texto = texto + "\n\nPREÇO:\n" + precoValor + " à vista ou 10x de " + parcelaValor;
                            texto = texto + "\nDESCRIÇÃO:\n" + descTexto;

                            JTextArea desc = new JTextArea(texto);
                            JLabel nome = new JLabel(nomeTexto);
                            JLabel preco = new JLabel("Preço: " + precoValor);
                            JLabel parcela = new JLabel(parcelaValor);

                            desc.setFont(new Font("ARIAL", Font.BOLD, 12));

                            desc.setEditable(false);

                            JScrollPane jsp2 = new JScrollPane(desc);

                            ImageIcon img = new ImageIcon("img//" + pd.getNomeImagem() + ".jpg");
                            img.setImage(img.getImage().getScaledInstance(200, 200, 100));
                            JLabel Imagem = new JLabel();
                            Imagem.setIcon(img);

                            painelCentral2.add(Imagem, BorderLayout.WEST);
                            painelCentral2.add(jsp2, BorderLayout.CENTER);

                        }
                        catch (IOException ex) {
                            Logger.getLogger(JanelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
            }

            painelCentral.add(new JLabel("Total de Resultados: " + Integer.toString(listaProdutos.size())));
        }
    }

    //Método que cria no Menu no topo da tela inicial
    public JMenu criarMenu() {
        menu = new JMenu("Opções");
        menu.setMnemonic('O');
        menuItem = new JMenuItem("Histórico");
        menuItem.setMnemonic('H');
        menuItem2 = new JMenuItem("Sair");
        menuItem2.setMnemonic('S');
        menuItem.addActionListener(new abrirHistorico());
        menuItem2.addActionListener(new sair());

        menu.add(menuItem);
        menu.add(menuItem2);
        return menu;
    }
    
    //Método que abre a janela do Histórico ao clicar no submenu 'Histórico'
    private static class abrirHistorico implements ActionListener {
        JanelaHistorico jh;
        public void actionPerformed(ActionEvent e) {
            try {
                jh = new JanelaHistorico();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }
    
    //Método que fecha a tela inicial ao clicar no submeno 'sair'
    private static class sair implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
}
