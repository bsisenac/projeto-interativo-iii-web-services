package webservice.rc;

import java.awt.*;
import javax.swing.*;

public class Janela {
Container container;
static JFrame janela;
static JPanel painelTopo;
static JPanel painelCentral;
static JPanel painelRodape;


//Construtor que monta a Janela default
public Janela(String nome) {
        janela = new JFrame(nome);
        janela.setSize(1200, 670);
        
        container = janela.getContentPane();
        container.setLayout(new BorderLayout());
        
        painelTopo = new JPanel();
        painelCentral = new JPanel();
        painelRodape = new JPanel();
        painelCentral.setLayout(new GridLayout(28, 2));
        
        painelCentral.setBackground(Color.WHITE);
        painelTopo.setBackground(new Color(227, 227, 227));
        painelRodape.setBackground(new Color(227, 227, 227));
        
        container.add(painelTopo, BorderLayout.NORTH);
        container.add(painelCentral, BorderLayout.CENTER);
        container.add(painelRodape, BorderLayout.SOUTH);
        
        ImageIcon logo = new ImageIcon("img//logoWaz.gif");
        
        JLabel Imagem = new JLabel();
        Imagem.setSize(100, 100);
        Imagem.setIcon(logo);
        painelTopo.add(Imagem, BorderLayout.PAGE_START);
        
        painelRodape.add(new JLabel("NITEIP™ Team | FANÁTICOS POR TECNOLOGIA | Copyright © 2014"));
        
        janela.setVisible(true);
    }
}

