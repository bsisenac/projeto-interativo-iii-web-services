package webservice.rc;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Historico implements Serializable {

    //Método que serializa e salva o arquivo 'serializable.dat'
    public void salvarLista(ListaLigada lista) {
        try {
            FileOutputStream saidaArquivo = new FileOutputStream("serializable.dat");
            ObjectOutputStream saiObjeto = new ObjectOutputStream(saidaArquivo);

            saiObjeto.writeObject(lista);
            saiObjeto.flush();
            saiObjeto.close();

            System.err.println("Arquivos Serializados.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Método que descerializa e le o arquivo 'serializable.dat'
    public ListaLigada lerArquivo() {

        ListaLigada lista = null;
        if (new File("serializable.dat").exists()) {
            try {
                //Carrega o arquivo
                FileInputStream arquivoLeitura = new FileInputStream("serializable.dat");
                ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
                
                lista = (ListaLigada) objLeitura.readObject();
                objLeitura.close();
                arquivoLeitura.close();
                
                return lista;
                
            } catch (IOException ex) {
                Logger.getLogger(Historico.class.getName()).log(Level.SEVERE, null, ex);
                return null;
                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Historico.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
        return lista;
    }
}
