package webservice.rc;

import java.io.Serializable;

public class No implements Serializable {
    public Produto dado;
    public No prox;
    
    public No(Produto valor) {
        prox = null;
        dado = valor;
    }
}
