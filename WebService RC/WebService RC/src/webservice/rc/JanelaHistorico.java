package webservice.rc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import static webservice.rc.Janela.painelTopo;
import static webservice.rc.JanelaProduto.painelCentral2;

public class JanelaHistorico extends Janela {

    public ArrayList<JButton> buttons;

    //Método que monta a Janela de Histórico
    public JanelaHistorico() throws Exception {
        super("WEB-SERVICE | HISTÓRICO");

        ListaLigada lista = new Historico().lerArquivo();

        if (lista == null) {
            throw new Exception("Histórico não preenchido.");
        }
        No aux = lista.getInicio();

        while (aux != null) {
            
            final Produto prod = (Produto) aux.dado;

            JButton botao = new JButton("VISUALIZAR PRODUTO");
            botao.setPreferredSize(new Dimension(5, 10));
            botao.setBackground(new Color(108, 163, 50));
            botao.setForeground(Color.WHITE);
            String[] colunas = {"Produtos", "Preço"};
            Object[][] dados = {{prod.getNome(), prod.getPreco()}};
            JTable tabela = new JTable(dados, colunas);

            tabela.setEnabled(false);

            painelCentral.add(tabela);
            painelCentral.add(botao);
            aux = aux.prox;

            painelTopo.validate();
            janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            
            //Ação para abrir a Janela do Produto ao clicar no botao
            botao.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    new JanelaProduto(prod.getNome());
                    String descTexto = prod.getDescricao();
                    String nomeTexto = prod.getNome();
                    String precoValor = prod.getPreco();
                    String parcelaValor = prod.getPrecoParcela();

                    String texto = "PRODUTO:\n" + nomeTexto;
                    texto = texto + "\n\nPREÇO:\n" + precoValor + " à vista ou 10x de " + parcelaValor;
                    texto = texto + "\nDESCRIÇÃO:\n" + descTexto;

                    JTextArea desc = new JTextArea(texto);
                    JLabel nome = new JLabel(nomeTexto);
                    JLabel preco = new JLabel("Preço: " + precoValor);
                    JLabel parcela = new JLabel(parcelaValor);

                    desc.setFont(new Font("ARIAL", Font.BOLD, 12));

                    desc.setEditable(false);

                    JScrollPane jsp2 = new JScrollPane(desc);

                    ImageIcon img = new ImageIcon("img//" + prod.getNomeImagem() + ".jpg");
                    img.setImage(img.getImage().getScaledInstance(200, 200, 100));
                    JLabel Imagem = new JLabel();
                    Imagem.setIcon(img);

                    painelCentral2.add(Imagem, BorderLayout.WEST);
                    painelCentral2.add(jsp2, BorderLayout.CENTER);
                }
            });
        }
        janela.setVisible(true);
    }
}
