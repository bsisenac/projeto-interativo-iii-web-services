package webservice.rc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class JanelaProduto {
    Container container2;
    static JFrame janela;
    static JPanel painelTopo2;
    static JPanel painelCentral2;
    static JPanel painelRodape2;
    Busca busca = new Busca();
    
    
    //Método que constrói a Jenal do produto 
    public JanelaProduto(String nome){
        janela = new JFrame(nome);
        janela.setSize(800, 500);
        
        container2 = janela.getContentPane();
        container2.setLayout(new BorderLayout());
        
        painelTopo2 = new JPanel();
        painelCentral2 = new JPanel();
        painelRodape2 = new JPanel();
        painelCentral2.setLayout(new BorderLayout());
         
        painelCentral2.setBackground(Color.WHITE);
        painelTopo2.setBackground(new Color(227, 227, 227));
        painelRodape2.setBackground(new Color(227, 227, 227));
      
        ImageIcon logo = new ImageIcon("img//logoWaz.gif");
        
        JLabel Imagem = new JLabel();
        Imagem.setSize(100, 100);
        Imagem.setIcon(logo);
        painelTopo2.add(Imagem, BorderLayout.PAGE_START);
        painelTopo2.add(new JLabel("\t\t\tDETALHES DO PRODUTO"), BorderLayout.EAST);
        painelRodape2.add(new JLabel("NITEIP™ Team | FANÁTICOS POR TECNOLOGIA | Copyright © 2014"));
     
        JScrollPane jsp = new JScrollPane(painelCentral2);  
        container2.add(jsp);
        
        container2.add(painelTopo2, BorderLayout.NORTH);
        container2.add(painelCentral2, BorderLayout.CENTER);
        container2.add(painelRodape2, BorderLayout.SOUTH);
        
        janela.setVisible(true);
    }
}
