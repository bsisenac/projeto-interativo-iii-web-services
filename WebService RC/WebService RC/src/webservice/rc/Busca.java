package webservice.rc;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class Busca {
    
    //construtor
    public Busca() {
    }
    
    
    //Primeira busca e consulta ao site via HttpGet
    //Retorna ArrayList
    public ArrayList<Produto> BuscaWaz(String entrada) throws ClientProtocolException, IOException {
        
        DefaultHttpClient httpclient = new DefaultHttpClient(); 
        entrada = entrada.replace(" ", "+");
        ArrayList<Produto> ar = new ArrayList<Produto>();
        HttpGet httpGet = new HttpGet("http://www.waz.com.br/catalogsearch/result/index/?d=1&limit=27&q="+entrada);
                
        try {
            ResponseHandler<String> manipulador = new BasicResponseHandler();
            String resposta = httpclient.execute(httpGet,manipulador);
            int inicio = resposta.indexOf("id=\"products-list\"");
            
            Produto p = new Produto();
            
            while(inicio > 0){
                resposta = resposta.substring(inicio);
                
                p.setNome(this.Nome(resposta));
                p.setPreco(this.Preco(resposta));
		p.setLink(this.Link(resposta));
                
                ar.add(p);
                p = new Produto();
                
                System.out.println();
                resposta = resposta.substring(resposta.indexOf("li alt")+1);
                inicio = resposta.indexOf("li alt");
            }
        }
        finally {
            httpGet.releaseConnection();
        }
        return ar;
    }

    //Indexa a String do Nome do produto
    private String Nome(String resposta) {
        String produto = resposta.substring(resposta.indexOf("product-name show-tooltip"), resposta.indexOf("</h3>"));
        produto = produto.substring(produto.indexOf("\">") + "\">".length(), produto.indexOf("</a>"));

        produto = produto.replace("\n", "");
        produto = produto.replace("	", "  ");
        produto = produto.replace("\t", "  ");
        produto = produto.replace("  ", "");

        System.out.println( "Nome: " + produto );
        return produto;
    }
    
    //Indexa a String do Preço do produto
    private String Preco(String resposta) {
        String produto = resposta.substring(resposta.indexOf("regular-price"), resposta.indexOf("<!------------------------>"));
        produto = produto.substring(produto.indexOf("R$") + "R$".length() - 2, produto.indexOf("</span>"));

        System.out.println( "Preço: " + produto );
        return produto;
    }

    //Indexa a String do link do produto
    private String Link(String resposta) {
        String produto = resposta.substring(resposta.indexOf("product-name show-tooltip"), resposta.indexOf("</h3>"));
        produto = produto.substring(produto.indexOf("<a href=") + "<a href=".length() +1 , produto.indexOf(".html") +5);

        System.out.println("Link: " + produto);
        return produto;
    }

}
