package webservice.rc;

//import java.io.IOException;
//import java.util.ArrayList;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class BuscaDetalheProduto {
   
	public BuscaDetalheProduto() {
            System.out.println("buscou..");
	}

        public Produto buscaPagina(String link) throws IOException {
            Produto p;
            
//            Imagem(link);
            DefaultHttpClient httpclient = new DefaultHttpClient(); 
            HttpGet httpGet = new HttpGet(link);
            System.out.println(link);
            
        try {
            ResponseHandler<String> manipulador = new BasicResponseHandler();
            String resposta = httpclient.execute(httpGet,manipulador);
            int inicio = resposta.indexOf("main-container");
            
            p = new Produto();
            
            resposta = resposta.substring(inicio);
            
            p.setDescricao(this.lerDescricao(resposta));
            p.setImagem(this.Imagem(resposta));    
            p.setNomeImagem( link.substring( p.getImagem().indexOf("br/") + 3, link.indexOf(".html")));
            
            baixarImagem(p.getImagem(), p.getNomeImagem());
            
//            p.setNome(this.Nome(resposta));
//            p.setPreco(this.Preco(resposta));
//            p.setLink(this.Link(resposta));
        }
        finally {
            httpGet.releaseConnection();
        }
        return p;
        }
        
        

	public String lerDescricao(String resposta) throws ClientProtocolException,IOException {
                String produto = resposta.substring(resposta.indexOf("<div id=\"detail-data-description\" class=\"box-collateral box-description actived\">"), resposta.indexOf("<!-- eo .box-collateral -->"));
                produto = produto.substring(produto.indexOf("<br />") +"<br />".length(), produto.indexOf("</div>"));
                
                produto = produto.replace("<br />", "\n");
                produto = produto.replaceAll("\\<.*?\\>", "");
                
                System.out.println( "Descrição: " + produto );
		return produto;
	}
        
                private String Preco(String resposta) {
                String produto = resposta.substring(resposta.indexOf("regular-price"), resposta.indexOf("<!------------------------>"));
		produto = produto.substring(produto.indexOf("R$") + "R$".length() - 2, produto.indexOf("</span>"));
            
                System.out.println( "Preço: " + produto );
		return produto;
	}
	
		private String Imagem(String resposta) {
                String produto = resposta.substring(resposta.indexOf("product-img-box"), resposta.indexOf("Duplo clique para aumentar imagem"));
		produto = produto.substring(produto.indexOf("<a href=\"") + "<a href=\"".length() , produto.indexOf(".jpg") +4);
//                produto = produto.substring(produto.indexOf("<img id=\"image\" src=\"") + "<img id=\"image\" src=\"".length() , produto.indexOf(".jpg") +4);
                
                System.out.println("Imagem: " + produto);
                
                return produto;
	}
                public String lerImagem(String link) throws ClientProtocolException,IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(link);
		String resposta = new String();
		try {
			ResponseHandler<String> manipulador = new BasicResponseHandler();
			resposta = httpclient.execute(httpGet, manipulador);
                        
			int inicio = resposta.indexOf("zoomPad");
			
			if (resposta.indexOf("src=\"") != -1) {
				resposta = resposta.substring(inicio);
				resposta = resposta.substring(0,resposta.indexOf("</div>"));
				
				resposta = resposta.substring(resposta.indexOf("src=\"")+5);
				resposta = resposta.substring(0,resposta.indexOf("\""));
				
				resposta = "http://www.waz.com.br" + resposta;			
			}
			
			System.out.println(resposta);
                        
		} finally {
			httpGet.releaseConnection();
		}
                
		return resposta;
	}
                

    public void baixarImagem(String urlProduto, String produto) throws MalformedURLException, IOException {
        URL url = new URL(urlProduto);
        InputStream is = url.openStream();
        
        OutputStream os = new FileOutputStream("img\\" + produto + ".jpg");
        
        byte[] b = new byte[2048];
        int length;
        
        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }
        
        is.close();
        os.close();
    }
	    
}