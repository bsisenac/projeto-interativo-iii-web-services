package webservice.rc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Historico{
	
	public void salvarLista(ListaLigada lista) {
		try {
		FileOutputStream saidaArquivo = new FileOutputStream(
				"serializable.dat");

		ObjectOutputStream saiObjeto = new ObjectOutputStream(saidaArquivo);
 
		saiObjeto.writeObject(lista);
		System.out.println("Arquivos Serializados");
		saiObjeto.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public String lerHistorico() {
		String conteudo = null;
		BufferedReader br = null;

		try { // puxar o arquivo

			String linha;
			StringBuilder paragrafo = new StringBuilder(); // construção da String
			br = new BufferedReader(new FileReader("historico.txt")); // carregamento do arquivo

			while ((linha = br.readLine()) != null) // leitura da linha
			{
				paragrafo.append(linha + "\n");
			}

			conteudo = paragrafo.toString(); // Arquivo transformado em String
		} catch (IOException e) { // verifica erro
			e.printStackTrace(); // imprimir erro
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace(); // imprimir erro
			}
		}
		return conteudo;
	}
}
//public class Historico {
//    public static String pathHistorico = "./Historico/historico.cfg"; 
//Será alterado para .ser, quando a ListaLigada tiver trazer os resultados em objetos serializados.
//    public static String pathSaida = "./Historico/historicoSaida.cfg"; 
//Temporário
//    
//    public static void abrir() {
//        String saida = "";
//        
//        try {
//            System.out.println("(" + pathHistorico + ")");
//            
//            FileReader reader = new FileReader(pathHistorico);
//            BufferedReader leitor = new BufferedReader(reader);
//
//            String linha = leitor.readLine();
//            
//            while (!linha.equals("0")) {
//                if (!linha.equals("")) {
//                    saida = saida + linha + "\n";
//                    linha = leitor.readLine();
//                }
//                else {
//                    saida = saida + "\n";
//                    linha = leitor.readLine();
//                }
//            }
//            String nohtml = saida.toString().replaceAll("\\<.*?>","");
//            System.out.println(nohtml);
//            //System.out.println(saida);
//            salvar(nohtml);
//        }
//        catch (IOException e){
//            System.out.println("Erro na Leitura do Arquivo.\n" + e.getMessage());
//        }
//    }
//    
//    public static void salvar(String nohtml) {
//        //Fazer a leitura de todos arquivos .cfg, ou do atual produto sendo presquisado e salvará em historico.cfg
//        //Ex: Produto1.cfg + Produto2.cfg = Histórico.cfg (Tratado)
//        
//        nohtml = nohtml + "0";
//        
//        try {
//            PrintWriter writer = new PrintWriter(pathSaida);
//            writer.print(nohtml);
//            writer.close();
//            System.out.println("Arquivo criado com succeso. (Historico.cfg)");
//        }
//        catch (FileNotFoundException e){
//            System.out.println("Erro no Salvamento do Arquivo.\n" + e.getMessage());
//        }
//    }
//}
