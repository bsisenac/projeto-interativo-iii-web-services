package webservice.rc;

//import java.io.IOException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class Busca {
    
    public Busca() {
    }
    
    public ArrayList<Produto> BuscaWaz(String entrada) throws ClientProtocolException, IOException {
        
        DefaultHttpClient httpclient = new DefaultHttpClient(); 
        entrada = entrada.replace(" ", "+");
        ArrayList<Produto> ar = new ArrayList<Produto>();
        HttpGet httpGet = new HttpGet("http://www.waz.com.br/catalogsearch/result/index/?d=1&limit=27&q="+entrada);
                
        try {
            ResponseHandler<String> manipulador = new BasicResponseHandler();
            String resposta = httpclient.execute(httpGet,manipulador);
            int inicio = resposta.indexOf("id=\"products-list\"");
            
            Produto p = new Produto();
            
            while(inicio > 0){
            	resposta = resposta.substring(inicio);
                
                p.setNome(this.Nome(resposta));
                p.setPreco(this.Preco(resposta));
		p.setDescricao(this.Descricao(resposta));
		p.setLink(this.Link(resposta));
                p.setImagem(this.Imagem(resposta));
                
                ar.add(p);
                p = new Produto();
                
                System.out.println();
                resposta = resposta.substring(resposta.indexOf("li alt")+1);
                inicio = resposta.indexOf("li alt");
            }
        }
        finally {
            httpGet.releaseConnection();
        }
        return ar;
    }
    
    public ArrayList<String> buscarImagens(String entrada) throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		entrada = entrada.replace(" ", "+");
		HttpGet httpGet = new HttpGet("http://www.waz.com.br/catalogsearch/result/?q="+ entrada);
		ArrayList<String> ar = new ArrayList<String>();
		try {
			ResponseHandler<String> manipulador = new BasicResponseHandler();
			String resposta = httpclient.execute(httpGet, manipulador);

			int inicio = resposta.indexOf("item");

			while (inicio != -1) {
				resposta = resposta.substring(inicio);

				ar.add(this.Imagem(resposta));

				System.out.println();
				resposta = resposta.substring(resposta.indexOf("item") + 1);
				inicio = resposta.indexOf("item");
			}
		} finally {
			httpGet.releaseConnection();
		}

		return ar;
	}

	private String Nome(String resposta) {
            
		String produto = resposta.substring(resposta.indexOf("product-name show-tooltip"), resposta.indexOf("</h3>"));
		produto = produto.substring(produto.indexOf("\">") + "\">".length(), produto.indexOf("</a>"));
                
                produto = produto.replace("	", "  ");
                produto = produto.replace("\t", "  ");
                produto = produto.replace("  ", "");
                
                System.out.println( "Nome: " + produto );
		return produto;
	}
        
        private String Preco(String resposta) {
                String produto = resposta.substring(resposta.indexOf("regular-price"), resposta.indexOf("<!------------------------>"));
		produto = produto.substring(produto.indexOf("R$") + "R$".length() - 2, produto.indexOf("</span>"));
            
                System.out.println( "Preço: " + produto );
		return produto;
	}

	private String Descricao(String resposta) {
            
                String produto = resposta.substring(resposta.indexOf("product-shortdescription"));
                produto = produto.substring(produto.indexOf("\"product-attributtes\"" )+ "\"product-attributtes\"".length()  );
                produto = produto.substring(produto.indexOf("\"product-attributtes\"" )+ "\"product-attributtes\"".length()  );
                produto = produto.substring(produto.indexOf("\"product-attributtes\"" )+ "\"product-attributtes\"".length()  );
                produto = produto.substring(produto.indexOf("\"product-attributtes\"" )+ "\"product-attributtes\"".length()  );
                
		produto = produto.substring(produto.indexOf("</strong>")+"</strong>".length(), produto.indexOf("</li>"));
                produto = produto.replace("	", "  ");
                produto = produto.replace("\t", "  ");
                produto = produto.replace("  ", "");
                System.out.println( "Descrição: " + produto );
		return produto;
	}
	
	private String Link(String resposta) {
                //String produto = "Link: testes";
		String produto = resposta.substring(resposta.indexOf("product-name show-tooltip"), resposta.indexOf("</h3>"));
		produto = produto.substring(produto.indexOf("<a href=") + "<a href=".length() +1 , produto.indexOf(".html") +5);
                //produto = produto.substring(produto.indexOf("R$") + "R$".length() - 2, produto.indexOf("</span>"));
		//produto = produto.substring(produto.indexOf("=\"")+2, produto.indexOf("\">"));
                
                
                produto = produto.replace("	", "  ");
                produto = produto.replace("\t", "  ");
                produto = produto.replace("  ", "");
		System.out.println("Link: " + produto);
		return produto;
	}

	private String Imagem(String resposta) {
		//String produto = "Link: testes";
                String produto = resposta.substring(resposta.indexOf("image-box"), resposta.indexOf("</div>"));
		produto = produto.substring(produto.indexOf("<img src=") + "<img src=".length() +1 , produto.indexOf(".jpg") +4);
//		if (resposta.indexOf("<img src=\"") != -1 ){
//			produto = resposta.substring(resposta.indexOf("image-box"),
//                                resposta.indexOf("</a>"));
//			produto = produto.substring(produto.indexOf("<img src=\"")
//                                + 10, produto.indexOf("\" width="));
//			produto = "http://www.waz.com.br" + produto;
//			
//		}
//		produto = "imagem1";
                System.out.println("Imagem: " + produto);
		return produto;
	}
}
