package webservice.rc;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import org.apache.http.client.ClientProtocolException;
import static webservice.rc.JanelaProduto.*;

public class JanelaPrincipal extends Janela {
    
	private JMenu menu;
        private JMenuItem menuItem, menuItem2;
        private JMenuBar menuBar;
        public JButton buscar;
        public JButton [] buttons;
	public static JTextField texto;
	private ArrayList<Produto> listaProdutos;
	private ArrayList<String> listaImagens;
	private Busca busca;
        
        final Historico historico = new Historico();
        final ListaLigada lista = new ListaLigada();
        
        private String []links;
        private String []nomes;
	
	public JanelaPrincipal() throws ClientProtocolException, IOException {
		super("PROJETO INTERATIVO III | WEB-SERVICE | WWW.WAZ.COM"); // nome da janela
		this.buscar = new JButton("Buscar"); // criar o botao busca
                buscar.setBackground(new Color(108, 163, 50));  
                buscar.setForeground(Color.WHITE);
		//JButton bHistorico = new JButton("Histórico"); // criar o botao historico
		texto = new JTextField("Digite o produto que deseja..."); // texto da caixa de pesquisa
                texto.setPreferredSize(new Dimension(500, 25));
		this.busca = new Busca();
		this.listaProdutos = new ArrayList<Produto>();
                
                
                janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
                
                menuBar = new JMenuBar();
                menu = criarMenu();
                menuBar.add(menu);
                
                janela.setJMenuBar(menuBar);
		
		texto.setColumns(30); // largura da caixa de pesquisa
		painelTopo.add(texto); // adiciona o texto na caixa de pesquisa
		painelTopo.add(buscar, BorderLayout.EAST); // adiciona o botão buscar
		//painelTopo.add(bHistorico); // adiciona o botão buscar

		// botoes dos produtos
		final String produto[] = new String[45];
                
		texto.addMouseListener(new MouseAdapter(){
                    public void mouseClicked(MouseEvent e){
                        texto.setText("");
                    }
                });
                
                texto.addKeyListener(new KeyAdapter() {
                    public void keyReleased(KeyEvent e) {
                        if(e.getKeyCode() == KeyEvent.VK_ENTER){
                            //painelCentral.add(new JLabel("Buscando..."));
                            eventoBotao();
                        }
                    }
                });
                
		buscar.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        //painelCentral.add(new JLabel("Buscando..."));
                        eventoBotao();
                    }
                });
		
		
		menuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				janela = new JFrame();
				janela.setSize(1365, 730);

				container = janela.getContentPane();
				
				painelTopo = new JPanel();		
				painelTopo.setBackground(Color.white);
		
				container.add(painelTopo);
				
				JLabel h = new JLabel(historico.lerHistorico());
				
				painelTopo.add(h);
				
				painelTopo.validate();
				
				janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				janela.setVisible(true);
			
			}
		});
		
		System.out.println("WebService iniciado.");
		
		JScrollPane scroller = new JScrollPane(painelCentral);
		this.container.add(scroller);
		
		painelTopo.validate();
		painelCentral.validate();
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
        
    public void eventoBotao() {
        
            //Tratando JTextField
        if (texto.getText().equals("") || texto.getText().equals("Digite o produto que deseja...")) {
            painelCentral.removeAll();
            painelCentral.repaint();
            painelCentral.revalidate();
            painelCentral.add(new JLabel("Busca Vazia."));
            System.err.println("Busca vazia.");
        }
        else {
            Produto tmp = new Produto();            
            
            painelCentral.removeAll();
            painelCentral.repaint();
            painelCentral.revalidate();

            listaProdutos = new ArrayList<Produto>();
            try {
                System.out.println("Buscando...\n");
                listaProdutos = busca.BuscaWaz(texto.getText());
                System.out.println("Total de Resultados: " + listaProdutos.size());
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Definindo tamanho do Array de Botões
            buttons = new JButton[listaProdutos.size()];
            Produto n = new Produto();
            
            links = new String[listaProdutos.size()];
            nomes = new String[listaProdutos.size()];

            for (int i = 0; i < buttons.length; i++){
                    n = (Produto) listaProdutos.get(i);
                    buttons[i] = new JButton("VISUALIZAR PRODUTO");
                    buttons[i].setPreferredSize(new Dimension(5, 10));
                    buttons[i].setBackground(new Color(108, 163, 50));  
                    buttons[i].setForeground(Color.WHITE);
                    String [] colunas = {  "Produtos", "Preço"  }; 
                    Object [][] dados = { { n.getNome(), n.getPreco() } };
                    JTable tabela = new JTable(dados, colunas);

                    //tabela.setPreferredSize(new Dimension(5, 15));
                    tabela.setEnabled(false);
                    
                    painelCentral.add(tabela);
                    links[i] = n.getLink();
                    nomes[i] = n.getNome();
                    painelCentral.add(buttons[i]);
            }

            for (int i = 0; i < buttons.length; i++) {
                final int num = i;
                buttons[i].addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e) {
                        try {
                            new JanelaProduto(nomes[num]);
                            //new JanelaProduto();                            
                            
                            //Nova Busca
                            BuscaDetalheProduto bdp = new BuscaDetalheProduto();
                            
                            Produto pd = bdp.buscaPagina(links[num]);
                            
                            String descTexto = pd.getDescricao();
                            //JLabel desc = new JLabel(pd.getDescricao());
                            JTextArea desc = new JTextArea(descTexto);
                            
                            ImageIcon img = new ImageIcon("img\\" + pd.getNomeImagem() + ".jpg");
                            
                            JLabel Imagem = new JLabel();
//                            Imagem.setSize(100, 100);
                            Imagem.setIcon(img);
                            
                            painelCentral2.add(Imagem, BorderLayout.WEST);
                            painelCentral2.add(desc, BorderLayout.CENTER);
//                        JButton src = (JButton) e.getSource();
//                        for (int h = 0; h < buttons.length; h++) {
//                            if (src==buttons[h]) {
//                                BuscaDetalheProduto bd = new BuscaDetalheProduto();
//                                String str = new String();
//                                try {
//                                    str = bd.lerDescricao(listaProdutos.get(h).getLink());
//                                } catch (IOException e2) {
//                                    e2.printStackTrace();
//                                }
//                                 historico.salvarLista(lista);
//                                try {
//                                    new JanelaProduto(listaProdutos.get(h));
//                                    System.out.println(listaImagens);
//                                } catch (IOException e1) {
//                                    e1.printStackTrace();
//                                }	
//                            }
//                        }
                        } catch (IOException ex) {
                            Logger.getLogger(JanelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                        }
                       
                    }
                });
            
        }            
            painelCentral.add(new JLabel("Total de Resultados: " + Integer.toString(listaProdutos.size())));
        }
    }

        public JMenu criarMenu() {
            menu = new JMenu("Opções");
            menu.setMnemonic('O');
            menuItem = new JMenuItem("Histórico");
            menuItem.setMnemonic('H');
            menuItem2 = new JMenuItem("Sair");
            menuItem2.setMnemonic('S');
            //menuItem.addActionListener(new abrirHistorico());
            menuItem2.addActionListener(new sair());

            menu.add(menuItem);
            menu.add(menuItem2);
            return menu;
    }
      
//    public static class abrirHistorico implements ActionListener{
//        public void actionPerformed(ActionEvent e) {
//                System.out.println("Abrindo...");
//                JanelaHistorico JH = new JanelaHistorico();
//            }
//    }
    
    private static class sair implements ActionListener {
        public void actionPerformed(ActionEvent e) {
                System.exit(0);
        }
    }
}
