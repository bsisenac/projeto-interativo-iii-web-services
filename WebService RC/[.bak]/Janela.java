package webservice.rc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;


public class Janela {
	static JFrame janela;
	Container container;
	static JPanel painelTopo;
	static JPanel painelCentral;
        static JPanel painelRodape;

	public Janela(String nome) {
		janela = new JFrame(nome);
		janela.setSize(1200, 700);

		container = janela.getContentPane();
		//container.setLayout(new GridLayout(2, 1));
                container.setLayout(new BorderLayout());

		painelTopo = new JPanel();
		painelCentral = new JPanel();
                painelRodape = new JPanel();
		painelCentral.setLayout(new GridLayout(28, 2));

		//painelRodape.setBackground(new Color(170, 170, 170));
                painelTopo.setBackground(new Color(227, 227, 227));
		painelCentral.setBackground(Color.WHITE);
                //painelCentral.setPreferredSize(new Dimension(100, 100)); 
                painelRodape.setBackground(new Color(227, 227, 227));
                
		container.add(painelTopo, BorderLayout.NORTH);
		container.add(painelCentral, BorderLayout.CENTER);
                container.add(painelRodape, BorderLayout.SOUTH);
                
		ImageIcon logo = new ImageIcon("img\\logoWaz.gif");

		JLabel Imagem = new JLabel();
		Imagem.setSize(100, 100);
		Imagem.setIcon(logo);
		painelTopo.add(Imagem, BorderLayout.PAGE_START);
                
                painelRodape.add(new JLabel("NITEIP™ Team | FANÁTICOS POR TECNOLOGIA | Copyright © 2014"));
	
		janela.setVisible(true);
	}
}

