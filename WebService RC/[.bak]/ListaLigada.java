package webservice.rc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ListaLigada{
	Historico h = new Historico();
	private No ini;

	public ListaLigada() {
		ini = null;
	}

	public void mostrarLista() {
		No aux = ini;
		while (aux != null) {
			System.out.println(aux.dado);
			aux = aux.prox;
		}
	}

	public void insereNoInicio(Object v) {
		No noNovo = new No(v);
		noNovo.prox = ini;
		ini = noNovo;
	}

	public void insereNoFim(Object v) {
		No noNovo = new No(v);
		if (ini == null)
			ini = noNovo;
		else {
			No aux = ini;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = noNovo;
		}
	}

	public void salvarHistorico(String Titulo, String noticia) {
		try {
			String copia = h.lerHistorico();

			File arquivoCriado = new File("historico.txt");
			if (!arquivoCriado.exists()) {
				arquivoCriado.createNewFile();
			}
			FileWriter escreverArq = new FileWriter(arquivoCriado);
			BufferedWriter carregarConteudo = new BufferedWriter(escreverArq);

			carregarConteudo.write(copia);
			carregarConteudo.write(" <*" + Titulo + "*>" + '\n'+ noticia +"\t" + "***");
			carregarConteudo.close();

			System.out.println("Arquivo salvo com exito");
			System.out.println(arquivoCriado.getAbsolutePath());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}