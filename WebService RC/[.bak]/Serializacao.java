package webservice.rc;

import java.io.Serializable;

public class Serializacao {

public class Produto implements Serializable {
	
	private String nome;
        private String preco;
	private String descricao;
        private String imagem;
	private String link;

	public Produto() {
		this.nome = new String();
                this.preco = new String();
		this.descricao = new String();
		this.imagem = new String();
	}
       
       
        public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

                public String getPreco() {
            return preco;
        }

        public void setPreco(String preco) {
            this.preco = preco;
        }
        
        public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
        
        public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
        
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

    }
}
